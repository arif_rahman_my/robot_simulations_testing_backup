#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["IMU",'/gurdy/imu/data', Imu],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Motion ---------'''
joint_list = [  ['head_upperlegM1_joint', '/gurdy/head_upperlegM1_joint_position_controller/command', [-0.8]],
                ['head_upperlegM2_joint', '/gurdy/head_upperlegM2_joint_position_controller/command', [-0.8]],
                ['head_upperlegM3_joint', '/gurdy/head_upperlegM3_joint_position_controller/command', [-0.8]],
                ['upperlegM1_lowerlegM1_joint', '/gurdy/upperlegM1_lowerlegM1_joint_position_controller/command', [0.4,]],
                ['upperlegM2_lowerlegM2_joint', '/gurdy/upperlegM2_lowerlegM2_joint_position_controller/command', [0.4,]],
                ['upperlegM3_lowerlegM3_joint', '/gurdy/upperlegM3_lowerlegM3_joint_position_controller/command', [0.4,]],
                ['head_upperlegM1_joint', '/gurdy/head_upperlegM1_joint_position_controller/command', [-1.2]],
                ['head_upperlegM2_joint', '/gurdy/head_upperlegM2_joint_position_controller/command', [-1.2]],
                ['head_upperlegM3_joint', '/gurdy/head_upperlegM3_joint_position_controller/command', [-1.2]],
                ['upperlegM1_lowerlegM1_joint', '/gurdy/upperlegM1_lowerlegM1_joint_position_controller/command', [-0.4,]],
                ['upperlegM2_lowerlegM2_joint', '/gurdy/upperlegM2_lowerlegM2_joint_position_controller/command', [-0.4,]],
                ['upperlegM3_lowerlegM3_joint', '/gurdy/upperlegM3_lowerlegM3_joint_position_controller/command', [-0.4,]],
                ['head_upperlegM1_joint', '/gurdy/head_upperlegM1_joint_position_controller/command', [-0.8]],
                ['head_upperlegM2_joint', '/gurdy/head_upperlegM2_joint_position_controller/command', [-0.8]],
                ['head_upperlegM3_joint', '/gurdy/head_upperlegM3_joint_position_controller/command', [-0.8]],
                ['upperlegM1_lowerlegM1_joint', '/gurdy/upperlegM1_lowerlegM1_joint_position_controller/command', [0.0,]],
                ['upperlegM2_lowerlegM2_joint', '/gurdy/upperlegM2_lowerlegM2_joint_position_controller/command', [0.0,]],
                ['upperlegM3_lowerlegM3_joint', '/gurdy/upperlegM3_lowerlegM3_joint_position_controller/command', [0.0,]],
                ['head_upperlegM1_joint', '/gurdy/head_upperlegM1_joint_position_controller/command', [0.0]],
                ['head_upperlegM2_joint', '/gurdy/head_upperlegM2_joint_position_controller/command', [0.0]],
                ['head_upperlegM3_joint', '/gurdy/head_upperlegM3_joint_position_controller/command', [0.0]],
                ]


for i in range(len(joint_list)):
    tm.print_header("\nTest Motion - {}".format(joint_list[i][0]))
    topic_command = joint_list[i][1]
    topic_state = '/gurdy/joint_states'
    joint_name = joint_list[i][0]
    point_list = joint_list[i][2]
    results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.1, wait_motion_time=1.0))


'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)