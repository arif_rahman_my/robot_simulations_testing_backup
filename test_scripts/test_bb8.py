#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''

tm.print_header("\nTest sensors. ")
sensor_list = [ ["Front RGB camera", '/bb8/camera1/image_raw', Image],
                #["Depth camera", '/head_camera/depth_downsample/image_raw', Image],
                ["IMU",'/bb8/imu/data', Imu],
              ]
results.append(tm.test_sensor(sensor_list))


'''---------- Test Motion ---------'''
'''
FOR SOME REASON, WHEN LOADED, BB8 FACES TO THE Y-AXIS. SO FORWARD MAKES IT GO ALONG Y-AXIS
TURNING IS ALSO OPPOSITE. SO THE SCRIPT HAS BEEN WRITTEN TO TAKE ALL THIS INTO ACCOUNT.
IT LOOKS WEIRD, BUT WORKS. 
'''
tm.print_header("\nTest Motion. ")

command_list = [    ["Pan right", Twist(linear=Vector3( 0.1, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Pan left", 	Twist(linear=Vector3(-0.1, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'bb_8', thres_pose_val=0.1, publish_count=25))

command_list = [    ["Stop",      Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'bb_8', thres_pose_val=0.2, publish_count=25)

command_list = [    ["Turn left",Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.3))], 
                    ["Turn right", Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.3))],
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'bb_8', thres_pose_val=0.1, publish_count=25))

command_list = [    ["Stop",      Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'bb_8', thres_pose_val=0.2, publish_count=25)

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)