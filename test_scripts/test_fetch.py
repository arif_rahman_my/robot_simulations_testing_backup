#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''

tm.print_header("\nTest sensors. ")
sensor_list = [ ["Laser sensor", '/base_scan', LaserScan],
                ["RGB camera", '/head_camera/rgb/image_raw', Image],
                ["Depth camera", '/head_camera/depth_registered/image_raw', Image],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Drive Motion ---------'''
tm.print_header("\nTest drive motion. ")

command_list = [    ["Forward",     Twist(linear=Vector3( 1.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Stop",        Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Reverse",     Twist(linear=Vector3(-1.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Turn left",   Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.8))], 
                    ["Turn right",  Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.8))],
                    ["Stop",        Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/base_controller/command','fetch'))

'''---------- Test Arm Motion ---------'''
tm.print_header("\nTest arm motion. ")
topic = "/arm_with_torso_controller/follow_joint_trajectory"

joint_names = ["torso_lift_joint", "shoulder_pan_joint",
               "shoulder_lift_joint", "upperarm_roll_joint",
               "elbow_flex_joint", "forearm_roll_joint",
               "wrist_flex_joint", "wrist_roll_joint"]

disco_poses = [[0.0  ,  1.5, -0.6,  3.0,  1.0,  3.0,  1.0,  3.0],
               [0.133,  0.8,  0.75, 0.0, -2.0,  0.0,  2.0,  0.0],
               [0.266, -0.8,  0.0,  0.0,  2.0,  0.0, -2.0,  0.0],
               [0.385, -1.5,  1.1, -3.0, -0.5, -3.0, -1.0, -3.0],
               [0.266, -0.8,  0.0,  0.0,  2.0,  0.0, -2.0,  0.0],
               [0.133,  0.8,  0.75, 0.0, -2.0,  0.0,  2.0,  0.0],
               [0.0  ,  1.5, -0.6,  3.0,  1.0,  3.0,  1.0,  3.0]]

results.append(tm.test_motion_FollowJointTrajectoryAction(topic, joint_names, disco_poses))

tm.print_header("\nTest head motion. ")
topic = "/head_controller/follow_joint_trajectory"
joint_names = ["head_pan_joint", "head_tilt_joint"]
joint_positions = [ [ 0.0, 0.0],
                    [ 0.5,-0.5],
                    [-0.5, 0.5],
                  ]
results.append(tm.test_motion_FollowJointTrajectoryAction(topic, joint_names, joint_positions))

tm.print_header("\nTest gripper motion. ")
topic = "gripper_controller/gripper_action"
gripper_positions = [ 0.00,
                      0.05,
                      0.10,
                    ]
results.append(tm.test_motion_GripperCommandAction(topic, gripper_positions))


'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)