#!/bin/bash
#!/usr/bin/env bash
#-----------------------------------------------------------------
# Running this script will:
# 1. Call up each test script and run it
# 2. Report its success / failure
#-----------------------------------------------------------------

# These Gazebo model path are only required when run locally
export GAZEBO_MODEL_PATH=/home/arif/catkin_ws/src/jackal/jackal_tools/jackal_tools/models:/home/arif/catkin_ws/src/tc_aibo/aibo_description/models:/home/arif/catkin_ws/src/person_sim/person_sim/models:/home/arif/catkin_ws/src/parrot_ardrone/sjtu_drone/models:$GAZEBO_MODEL_PATH

scripts=$(ls test_scripts/test_*.sh)

echo "Starting tests" > output.log

for i in $scripts; do
    echo $i 
    echo "\n" >> output.log
    echo $i >> output.log
    sh $i &
	test_pid=$!
	echo $test_pid >> output.log

	if wait $test_pid; then
		test_res=0
	    echo "$i PASS"
	    echo "$i PASS" >> output.log 
	else
		test_res=1
	    echo "$i FAIL"
	    echo "$i FAIL" >> output.log 
	fi
	sleep 1
done

echo "\n\nALL DONE"
echo "\n\nALL DONE" >> output.log 
