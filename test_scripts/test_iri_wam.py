#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["Laser sensor", '/laser_scan', LaserScan],
                ["RGB camera", '/camera/rgb/image_raw', Image],
                ["Depth camera", '/camera/depth/image_raw', Image],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Motion ---------'''
tm.print_header("\nTest motion. ")
topic_command = '/iri_wam/iri_wam_controller/command'
topic_state = '/iri_wam/iri_wam_controller/state'
joint_names = ['iri_wam_joint_1', 'iri_wam_joint_2', 'iri_wam_joint_3', 'iri_wam_joint_4', 'iri_wam_joint_5', 'iri_wam_joint_6', 'iri_wam_joint_7']
point_list = [  [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                [-0.5, -0.5, -0.5, -0.5, -0.5, -0.5, -0.5],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
             ]

results.append(tm.test_motion_JointTrajectory(topic_command, topic_state, joint_names, point_list))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)