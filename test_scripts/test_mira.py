#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["RGB camera", '/mira/mira/camera1/image_raw', Image],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Motion ---------'''
tm.print_header("\nTest motion - pitch. ")
topic_command = '/mira/pitch_joint_position_controller/command'
topic_state = '/mira/joint_states'
joint_name = 'pitch_joint'
point_list = [ 0.2,
              #-0.2,  # Mira can't do this
               0.0,
             ]

results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list))

tm.print_header("\nTest motion - roll. ")
topic_command = '/mira/roll_joint_position_controller/command'
topic_state = '/mira/joint_states'
joint_name = 'roll_joint'
point_list = [ 0.2,
              -0.2,
               0.0,
             ]

results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list))

tm.print_header("\nTest motion - yaw. ")
topic_command = '/mira/yaw_joint_position_controller/command'
topic_state = '/mira/joint_states'
joint_name = 'yaw_joint'
point_list = [ 0.2,
              -0.2,
               0.0,
             ]

results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)