#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''

tm.print_header("\nTest sensors. ")
sensor_list = [ ["IMU",'/sphero/imu/data3', Imu],
              ]
results.append(tm.test_sensor(sensor_list))


'''---------- Test Motion ---------'''
tm.print_header("\nTest Motion. ")

command_list = [    ["Pan right", 	Twist(linear=Vector3( 0.01, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Pan left", 	Twist(linear=Vector3(-0.01, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Turn left", Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.2))], 
                    ["Turn right",Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.2))],
                    ["Stop", 		  Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'sphero'))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)