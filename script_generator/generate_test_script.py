#! /usr/bin/env python
'''
Generates test scripts based on inputs:
- name of template file
- name of output file
- command to launch Gazebo
- command to run test
'''

# Name of the template file
template = "template.sh"

# The output file name, and commands to be inserted
outputs = [ 
["../test_scripts/test_bb8.sh", "roslaunch bb_8_gazebo main.launch", "test_bb8.py"],
["../test_scripts/test_husky.sh", "roslaunch husky_gazebo husky_empty_world.launch", "test_husky.py"],
["../test_scripts/test_iri_wam.sh", "roslaunch iri_wam_gazebo main.launch", "test_iri_wam.py"],
["../test_scripts/test_parrot_ardrone.sh", "roslaunch drone_construct main.launch", "test_parrot_ardrone.py"],
["../test_scripts/test_sphero.sh", "roslaunch sphero_gazebo main.launch", "test_sphero.py"],
["../test_scripts/test_turtlebot.sh", "roslaunch turtlebot_gazebo main.launch", "test_turtlebot.py"],
["../test_scripts/test_summit_xl.sh", "roslaunch summit_xl_gazebo main.launch", "test_summit_xl.py"],
["../test_scripts/test_aibo.sh", "roslaunch aibo_description main.launch", "test_aibo.py"],
["../test_scripts/test_fetch.sh", "roslaunch fetch_gazebo main.launch", "test_fetch.py"],
["../test_scripts/test_mira.sh", "roslaunch mira_gazebo main.launch", "test_mira.py"],
["../test_scripts/test_rb1.sh", "roslaunch rb1_tc main.launch", "test_rb1.py"],
["../test_scripts/test_ur5.sh", "roslaunch ur_gazebo ur5.launch", "test_ur5.py"],
["../test_scripts/test_shadow_hand.sh", "roslaunch shadow_tc launch_simulation_hand.launch", "test_shadow_hand.py"],
["../test_scripts/test_pi_robot.sh", "roslaunch pi_robot_pkg test.launch", "test_pi_robot.py"],
["../test_scripts/test_rrbot.sh", "roslaunch rrbot_gazebo test.launch", "test_rrbot.py"],
["../test_scripts/test_gurdy.sh", "roslaunch gurdy_description main.launch", "test_gurdy.py"],
["../test_scripts/test_jibo.sh", "roslaunch jibo_description main.launch", "test_jibo.py"],
["../test_scripts/test_sia.sh", "roslaunch sia10f_gazebo main.launch", "test_sia.py"],
["../test_scripts/test_haro.sh", "roslaunch haro_description main_only_haro.launch", "test_haro.py"],
["../test_scripts/test_garbage_collector.sh", "roslaunch garbagecollector_description test.launch", "test_garbage_collector.py"],
["../test_scripts/test_autonomous_vehicle.sh", "roslaunch autonomous_vehicle_simple main.launch", "test_autonomous_vehicle.py"],
["../test_scripts/test_hector_quadrotor.sh", "roslaunch hector_quadrotor_gazebo camera_launch.launch", "test_hector_quadrotor.py"],
["../test_scripts/test_carbot.sh", "roslaunch carbot_gazebo_control carbot_empty_world_control.launch", "test_carbot.py"],
["../test_scripts/test_jackal.sh", "roslaunch jackal_course_outdoor main.launch", "test_jackal.py"],
]

# Lines numbers to substitute the above commands. 
# We -1 because count starts at 0, lines start at 1
out_line_1 = 12 -1
out_line_2 = 18 -1

def main():
    global template
    global outputs
    global out_line_1
    global out_line_2

    
    # Loop through each output
    for output in outputs:
        # Open the files
        template_file = open(template,"r")
        output_file = open(output[0],"w+")
        print "Working on {}".format(output[0])
        # Grab the contents of the template, and write to output file
        content = ""
        lines = template_file.readlines()
        for i,line in enumerate(lines):
            #print line
            # If line == out_line_1, print the command
            if i == out_line_1:
                output_file.write(output[1]+" &\n")
            # If line == out_line_2, print the command
            elif i == out_line_2:
                output_file.write("rosrun robot_simulations_testing "+output[2]+" &\n")
            # Else just copy from the template
            else:
                output_file.write(line)
        
        # Close the files
        template_file.close()
        output_file.close()
        
    print "DONE."

if __name__== "__main__":
    main()
