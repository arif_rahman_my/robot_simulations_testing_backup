#! /usr/bin/env python

import rospy
import math
from std_msgs.msg import Float64, Empty
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import LaserScan, Image, JointState, Range, NavSatFix
from geometry_msgs.msg import Twist, Vector3, Vector3Stamped
from nav_msgs.msg import Odometry
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from control_msgs.msg import JointTrajectoryControllerState, JointControllerState
import actionlib
from control_msgs.msg import (FollowJointTrajectoryAction,
                              FollowJointTrajectoryGoal,
                              GripperCommandAction,
                              GripperCommandGoal)
from colors import colors

'''
Class Test_Model
A series of functions to test the models in a Gazebo simulation. 

'''
class Test_Model(object):

    def __init__(self, debug=False):
        # initialize the node
        rospy.init_node('tester_gazebo') 
        self.debug = debug 

    def wait_for_gazebo_load(self):
        # Wait for Gazebo to load, then wait until sim time > 10s
        #print("Waiting for Gazebo to load. ")
        rospy.wait_for_service('/gazebo/get_world_properties')

        try:
            # Just call the service to unpause Gazebo. Some will load paused. 
            unpause_gazebo = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
            unpause_gazebo()
            # Get the sim time
            get_world_properties = rospy.ServiceProxy('/gazebo/get_world_properties', GetWorldProperties)
            simtime = 0.0
            self.print_message("Allow simulation to run for at least 10s.")
            while(simtime < 10.0):
                rospy.sleep(2)
                res = get_world_properties()
                simtime = res.sim_time
                self.print_message("Sim time:{}s".format(simtime))
            return True
                
        except rospy.ServiceException, e:
            self.print_message("Service call failed: {}".format(e))
            return False


    def test_sensor(self, sensor_list):
        result = True
        
        '''
        sensor_list = [ ["Laser sensor", '/laser_scan', LaserScan],
                        ["RGB camera", '/camera/rgb/image_raw', Image],
                        ["Depth camera", '/camera/depth/image_raw', Image],
                        ["Odom", '/odom', Twist]
                        ]
        '''
        for sensor in sensor_list:
            self.print_subheader("Testing {}".format(sensor[0]))
            msg_period = []
            last_time = rospy.Time.now()
            for i in range(11): # get 11 readings, but then remove the 1st one
                try:
                    msg = rospy.wait_for_message(sensor[1], sensor[2], timeout=2)
                    msg_period.append((rospy.Time.now()-last_time).to_sec())
                    last_time = rospy.Time.now()
                except(rospy.ROSException), e:
                    #print("Error message: ", e)
                    self.print_error(e)
            if(len(msg_period)==11):
                del msg_period[0]
                self.print_message("Approx rate: {}Hz".format(1/(sum(msg_period)/len(msg_period))))
                self.print_result_pass()
            else:
                result = False  
                self.print_result_failed()

        return result


    
    def test_motion_JointTrajectory(self, topic_command, topic_state, joint_names, point_list, thres_joint_position_val=0.01 ):
        result = True
        try:
            pub = rospy.Publisher(topic_command, JointTrajectory, queue_size=1)
            command = JointTrajectory()
            point = JointTrajectoryPoint()
            point.time_from_start.secs = 1 # controls how fast the robot moves towards the point
            command.points.append(point)
            wait_start_time = rospy.Time.now()
            self.print_message("Connecting to controller...")
            while(pub.get_num_connections() == 0):
                # assumes only Gazebo is waiting for this command
                # if there are others subscribing to this topic, maybe change to:
                # while(pub.get_num_connections() > 0 or 1 or 2):
                if(rospy.Time.now()-wait_start_time > rospy.Duration(10)):
                    raise rospy.ROSException("Controller not connected.") 
                
            '''
            command.joint_names = ['iri_wam_joint_1', 'iri_wam_joint_2', 'iri_wam_joint_3', 'iri_wam_joint_4', 'iri_wam_joint_5', 'iri_wam_joint_6', 'iri_wam_joint_7']
            point_list = [  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                            [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                            [-0.5, -0.5, -0.5, -0.5, -0.5, -0.5, -0.5]]
            '''
            command.joint_names = joint_names

            for i,point.positions in enumerate(point_list):
                self.print_subheader("Point {}:".format(i))
                # First send the command
                command.header.stamp = rospy.Time.now()
                self.print_message("Target position:{}".format(point.positions))
                pub.publish(command)
                rospy.sleep(3)
                # Second read the final joint positions
                msg = rospy.wait_for_message(topic_state, JointTrajectoryControllerState, timeout=2)
                self.print_message("Actual positions:{}".format(msg.actual.positions))
                # Calculate the difference between commanded and actual
                diff = [math.fabs(point.positions[i]-msg.actual.positions[i]) for i in range(len(point.positions))]
                # If difference is lower than threshold, PASS. Else FAIL
                if(max(diff)<thres_joint_position_val):
                    self.print_result_pass()
                else:
                    self.print_result_failed()
                    result = False
            
        except(rospy.ROSException), e:
            self.print_error(e)
            result = False

        return result

    def test_motion_FollowJointTrajectoryAction(self, action_topic, joint_names, point_list): 
        result = True
        try:
            self.print_message("Connecting to server...")
            client = actionlib.SimpleActionClient(action_topic, FollowJointTrajectoryAction)
            client.wait_for_server(timeout=rospy.Duration(10))
            self.print_message("... connected. ")
            command = JointTrajectory()
            point = JointTrajectoryPoint()
            point.time_from_start.secs = 3 # controls how fast the robot moves towards the point
            command.points.append(point)
            command.joint_names = joint_names

            for i,point.positions in enumerate(point_list):
                self.print_subheader("Point {}:".format(i))
                # First send the command
                goal = FollowJointTrajectoryGoal()
                goal.trajectory = command 
                goal.goal_time_tolerance = rospy.Duration(1.0)
                self.print_message("Target position:{}".format(point.positions))
                client.send_goal(goal)
                #rospy.sleep(5)
                client.wait_for_result(rospy.Duration(10.0))
                # Second read the final joint positions
                cres = client.get_result()
                if(cres.error_code == 0):
                    #print("Result: PASS")
                    self.print_result_pass()
                else:
                    #print("Result: FAILED")
                    self.print_message("Error code:{}".format(cres.error_code))
                    self.print_result_failed()
                    result = False
        except(rospy.ROSException), e:
            self.print_error(e)
            result = False

        return result
    


    def test_motion_GripperCommandAction(self, action_topic, point_list): 
        result = True
        try:
            self.print_message("Connecting to server...")
            client = actionlib.SimpleActionClient(action_topic, GripperCommandAction)
            client.wait_for_server(timeout=rospy.Duration(10))
            self.print_message("... connected. ")
            gripper_goal = GripperCommandGoal()
            gripper_goal.command.max_effort = 10.0

            for i,point in enumerate(point_list):
                self.print_subheader("Point {}:".format(i))
                # First send the command
                gripper_goal.command.position = point 

                self.print_message("Target position:{}".format(point))
                client.send_goal(gripper_goal)
                #rospy.sleep(5)
                client.wait_for_result(rospy.Duration(10.0))
                # Second read the final joint positions
                cres = client.get_result()
                
                if(cres.reached_goal == True):
                    self.print_result_pass()
                else:
                    self.print_result_failed()
                    result = False
                    
        except(rospy.ROSException), e:
            self.print_error(e)
            result = False

        return result


    def test_motion_Float(self, topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.05, wait_motion_time=3.0): 
        result = True
        try:
            pub = rospy.Publisher(topic_command, Float64, queue_size=1)
            command = Float64()
            
            wait_start_time = rospy.Time.now()
            self.print_message("Connecting to controller...")
            while(pub.get_num_connections() == 0):
                # assumes only Gazebo is waiting for this command
                # if there are others subscribing to this topic, maybe change to:
                # while(pub.get_num_connections() > 0 or 1 or 2):
                if(rospy.Time.now()-wait_start_time > rospy.Duration(10)):
                    raise rospy.ROSException("Controller not connected.") 
            
            for i,point in enumerate(point_list):
                self.print_subheader("Point {}:".format(i))
                # First send the command
                command.data = point 
                self.print_message("Target position:{}".format(command.data))
                pub.publish(command)
                rospy.sleep(wait_motion_time)
                # Second read the final joint positions
                msg = rospy.wait_for_message(topic_state, JointState, timeout=2)
                idx = msg.name.index(joint_name)
                #act = (msg.position[idx] % math.pi) if ((msg.position[idx] % math.pi)<math.pi/2) else ((msg.position[idx] % math.pi)-math.pi)
                act = msg.position[idx]
                # normalize the value [-pi, pi]
                act = act % (2*math.pi)
                if act > (math.pi):
                    act = act - 2*math.pi

                self.print_message("Actual positions:{}".format(act))
                # Calculate the difference between commanded and actual
                diff = math.fabs(point-act)
                # If difference is lower than threshold, PASS. Else FAIL
                if(diff<thres_joint_position_val):
                    #print("Result: PASS")
                    self.print_result_pass()
                else:
                    #print("Result: FAILED")
                    self.print_result_failed()
                    result = False
            
        except(rospy.ROSException), e:
            self.print_error(e)
            result = False

        return result



    def compare_twist(self, a, b):
        diff = []
        diff.append(math.fabs(a.linear.x - b.linear.x))
        diff.append(math.fabs(a.linear.y - b.linear.y))
        diff.append(math.fabs(a.linear.z - b.linear.z))
        diff.append(math.fabs(a.angular.x - b.angular.x))
        diff.append(math.fabs(a.angular.y - b.angular.y))
        diff.append(math.fabs(a.angular.z - b.angular.z))
        return diff 

    def compare_pose(self, a, b):
        diff = []
        diff.append(a.position.x - b.position.x)
        diff.append(a.position.y - b.position.y)
        diff.append(a.position.z - b.position.z)
        diff.append(a.orientation.x - b.orientation.x)
        diff.append(a.orientation.y - b.orientation.y)
        diff.append(a.orientation.z - b.orientation.z)
        return diff 

    def test_motion_Twist(self, command_list, topic, thres_twist_val=0.05):
        result = True
        try:
            pub = rospy.Publisher(topic, Twist, queue_size=1)
            wait_start_time = rospy.Time.now()
            self.print_message("Connecting to controller...")
            while(pub.get_num_connections() == 0):
                # assumes only Gazebo is waiting for this command
                # if there are others subscribing to this topic, maybe change to:
                # while(pub.get_num_connections() > 0 or 1 or 2):
                if(rospy.Time.now()-wait_start_time > rospy.Duration(10)):
                    raise rospy.ROSException("Controller not connected.") 
            self.print_message("Connected.")
            for command in command_list:
                self.print_subheader("Motion: "+command[0])
                self.print_message("Target:\n{}".format(command[1]))
                pub.publish(command[1])
                rospy.sleep(2)
                # do the test
                msg = rospy.wait_for_message('/odom', Odometry, timeout=2)
                #print msg.twist.twist 
                # Calculate the difference between commanded and actual
                diff = self.compare_twist(command[1], msg.twist.twist)
                # If difference is lower than threshold, PASS. Else FAIL
                self.print_message("Actual:\n{}".format(msg.twist.twist))
                #self.print_message("Diff = {}".format((diff)))
                if(max(diff)<thres_twist_val):
                    #print("Result: PASS")
                    self.print_result_pass()
                else:
                    #print("Result: FAILED.")
                    self.print_result_failed()
                    result = False 
        except(rospy.ROSException), e:
            #print "Error message: ", e
            self.print_error(e)
            result = False

        return result

    def test_motion_Twist_Position(self, command_list, topic, model_name, thres_pose_val=0.1, publish_count=15):
        result = True
        try:
            pub = rospy.Publisher(topic, Twist, queue_size=1)
            wait_start_time = rospy.Time.now()
            self.print_message("Connecting to controller...")
            while(pub.get_num_connections() == 0):
                # assumes only Gazebo is waiting for this command
                # if there are others subscribing to this topic, maybe change to:
                # while(pub.get_num_connections() > 0 or 1 or 2):
                if(rospy.Time.now()-wait_start_time > rospy.Duration(10)):
                    raise rospy.ROSException("Controller not connected.") 
            self.print_message("Connected.")
            for command in command_list:
                # Get the pose before motion command
                before = rospy.wait_for_message('/gazebo/model_states', ModelStates, timeout=2)
                idx = before.name.index(model_name)
                # Publish command
                self.print_subheader("Motion: "+command[0])
                #rate = rospy.Rate(5)
                for i in range(publish_count):
                    pub.publish(command[1])
                    #rate.sleep()
                    rospy.sleep(0.2) #5Hz
                
                # Get the pose after motion command
                after = rospy.wait_for_message('/gazebo/model_states', ModelStates, timeout=2)
                # Now compare. First calc the difference
                diff = self.compare_pose(after.pose[idx], before.pose[idx])
                #self.print_message("Before positions:{}".format(before.pose[idx]))
                #self.print_message("After positions:{}".format(after.pose[idx]))
                self.print_message("Displacements:{}".format(diff))
                # This is just so that we can divide by ~0
                if(before.pose[idx].position.x == 0.00):
                    before.pose[idx].position.x = 0.001
                if(before.pose[idx].position.y == 0.00):
                    before.pose[idx].position.y = 0.001
                if(before.pose[idx].position.z == 0.00):
                    before.pose[idx].position.z = 0.001
                if(before.pose[idx].orientation.z == 0.00):
                    before.pose[idx].orientation.z = 0.001
                # If forward / reverse, check if x-position has changed > 10%
                if(command[0] == "Forward"):
                    if(diff[0] > 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                elif(command[0] == "Reverse"):
                    if(diff[0] < 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                # If turn left / right, check if z-orientation has changed > 10%
                elif(command[0] == "Turn left"):
                    if(diff[5] > 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                elif(command[0] == "Turn right"):
                    if(diff[5] < 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                # If pan left / right, check if y-position has changed > 10%
                elif(command[0] == "Pan left"):
                    if(diff[1] > 0 and math.fabs(diff[1]/before.pose[idx].position.y) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                elif(command[0] == "Pan right"):
                    if(diff[1] < 0 and math.fabs(diff[1]/before.pose[idx].position.y) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                # If forward / reverse left / right, check x-position, and z-orientation has changed > 10%
                elif(command[0] == "Forward left"):
                    res = False 
                    if(diff[0] > 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        if(diff[5] > 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                            res = True
                elif(command[0] == "Forward right"):
                    res = False 
                    if(diff[0] > 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        if(diff[5] < 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                            res = True
                elif(command[0] == "Reverse left"):
                    res = False 
                    if(diff[0] < 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        if(diff[5] < 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                            res = True
                elif(command[0] == "Reverse right"):
                    res = False 
                    if(diff[0] < 0 and math.fabs(diff[0]/before.pose[idx].position.x) > thres_pose_val):
                        if(diff[5] > 0 and math.fabs(diff[5]/before.pose[idx].orientation.z) > thres_pose_val):
                            res = True
                # If ascend / descend, check if z-position has changed > 10%
                elif(command[0] == "Ascend"):
                    if(diff[2] > 0 and math.fabs(diff[2]/before.pose[idx].position.z) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                elif(command[0] == "Descend"):
                    if(diff[2] < 0 and math.fabs(diff[2]/before.pose[idx].position.z) > thres_pose_val):
                        res = True
                    else:
                        res = False 
                # If stop, check the 'after' speed
                elif(command[0] == "Stop"):
                    freeze = Twist()
                    diff = self.compare_twist(after.twist[idx], Twist())
                    #self.print_message((diff))
                    if(max(diff) < thres_pose_val):
                        res = True
                    else:
                        res = False 
                # Print the test result 
                if(res):
                    self.print_result_pass()
                else:
                    self.print_result_failed()
                    result = False 
        except(rospy.ROSException), e:
            self.print_error(e)
            result = False

        return result

    def publish(self, topic, data):
        pub = rospy.Publisher(topic, type(data), queue_size=1)
        wait_start_time = rospy.Time.now()
        self.print_message("Publishing to topic {}".format(topic))
        while(pub.get_num_connections() == 0):
            # assumes only Gazebo is waiting for this command
            # if there are others subscribing to this topic, maybe change to:
            # while(pub.get_num_connections() > 0 or 1 or 2):
            if(rospy.Time.now()-wait_start_time > rospy.Duration(10)):
                raise rospy.ROSException("Subscriber not connected.") 
        pub.publish(data)


    def print_header(self, msg):
        if(self.debug): 
            print(colors.bold+msg+colors.reset)
        else:
            rospy.loginfo(msg)

    def print_subheader(self, msg):
        if(self.debug): 
            print(colors.fg.lightblue+msg+colors.reset)
        else:
            rospy.loginfo(msg)

    def print_result_pass(self):
        if(self.debug): 
            print("Result: "+colors.bold+colors.fg.green+"PASS"+colors.reset)
        else:
            rospy.loginfo("Result: PASS")
    
    def print_result_failed(self):
        if(self.debug): 
            print("Result: "+colors.bold+colors.fg.lightgrey+colors.bg.red+"FAILED"+colors.reset)
        else:
            rospy.loginfo("Result: FAILED")
        
    def print_error(self, msg):
        if(self.debug): 
            print("Error: "+colors.fg.red+str(msg)+colors.reset)
        else:
            rospy.logerr(msg)

    def print_message(self, msg):
        if(self.debug): 
            print(msg)
        else:
            rospy.logdebug(msg)


            