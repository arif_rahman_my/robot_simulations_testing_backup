#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["Laser sensor", '/rrbot/laser/scan', LaserScan],
                ["RGB camera", '/rrbot/camera1/image_raw', Image],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Motion ---------'''
joint_list = [  ['joint1', '/rrbot/joint1_position_controller/command',	[1.57,-1.57,0.0]],
                ['joint2', '/rrbot/joint2_position_controller/command', [1.57,-1.57,0.0]],
            ]

for i in range(len(joint_list)):
    tm.print_header("\nTest motion - {}".format(joint_list[i][0]))
    topic_command = joint_list[i][1]
    topic_state = '/rrbot/joint_states'
    joint_name = joint_list[i][0]
    point_list = joint_list[i][2]
    results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.2, wait_motion_time=3.0))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)