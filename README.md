# Robot Simulation Testing #

Test scripts to ensure functionality of simulation models. 

The scripts will load the selected model into Gazebo, then test the sensor as well as the motion of the robot model. 

The sensors are tested simply with availability of messages. 

The motion are tested based on the type of robot. 
For robots with joints, the joints are moved and then measured. 
For robots with movable base, motion commands are sent and then monitored. 

Folder structure:

test_scripts:

1. actual test scripts are the Python files, named as "test_[name].py"
2. bash scripts to load Gazebo and run the Python script above are named as "test_[name].sh"

script_generator:

1. generate\_test\_scripts.py - a script to generate the bash scripts in batches. 
2. template.sh - a template of the "test_[name].sh" files, used by generate\_test\_scripts.py

run\_all\_tests.sh:

* iterates over all bash scripts in the folder test_scripts, running the test one-by-one


Arif Rahman arahman@theconstructsim.com

