#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Arm Motion ---------'''
tm.print_header("\nTest arm motion. ")
action_topic = "/hand_controller/follow_joint_trajectory"

joint_names = ["H1_F1J1", "H1_F1J2", "H1_F1J3", "H1_F2J1", "H1_F2J2", "H1_F2J3", "H1_F3J1", "H1_F3J2", "H1_F3J3"]

joint_positions = [ [ 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00],
                    [-0.79, 0.50, 0.50,-0.79, 0.50, 0.50, 0.79, 0.50, 0.50],
                    [-0.79,-1.00,-1.00, 0.00,-1.00,-1.00, 0.79,-1.00,-1.00],
                    [ 0.00, 1.05, 1.57, 0.00, 1.05, 1.57, 0.00, 1.05, 1.57],

                  ]
results.append(tm.test_motion_FollowJointTrajectoryAction(action_topic, joint_names, joint_positions))


'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)