#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["RGB camera", '/head_camera/rgb/image_raw', Image],
                ["Depth camera", '/head_camera/depth_registered/image_raw', Image],
              ]
results.append(tm.test_sensor(sensor_list))


'''---------- Test Motion ---------'''
joint_list = [  #['L_ear_joint', '/aibo_tc/L_ear_joint_position_controller/command', [0.1,0.0]],
                #['L_ear_tilt', '/aibo_tc/L_ear_tilt_position_controller/command',   [0.1,-0.1,0.0]],
                #['R_ear_joint', '/aibo_tc/R_ear_joint_position_controller/command', [0.1,0.0]],
                #['R_ear_tilt', '/aibo_tc/R_ear_tilt_position_controller/command',   [0.1,-0.1,0.0]],
                ['headPan', '/aibo_tc/headPan_position_controller/command',         [0.4,-0.4,0.0]],
                ['headTilt', '/aibo_tc/headTilt_position_controller/command',       [0.4,-0.4,0.0]],
                ['legLB1', '/aibo_tc/legLB1_position_controller/command',           [0.4,-0.4,0.0]],
                ['legLB2', '/aibo_tc/legLB2_position_controller/command',           [0.4,0.0]],
                ['legLB3', '/aibo_tc/legLB3_position_controller/command',           [0.4,0.0]],
                ['legLF1', '/aibo_tc/legLF1_position_controller/command',           [0.4,-0.4,0.0]],
                ['legLF2', '/aibo_tc/legLF2_position_controller/command',           [0.4,0.0]],
                ['legLF3', '/aibo_tc/legLF3_position_controller/command',           [0.4,0.0]],
                ['legRB1', '/aibo_tc/legRB1_position_controller/command',           [0.4,-0.4,0.0]],
                ['legRB2', '/aibo_tc/legRB2_position_controller/command',           [0.4,0.0]],
                ['legRB3', '/aibo_tc/legRB3_position_controller/command',           [0.4,0.0]],
                ['legRF1', '/aibo_tc/legRF1_position_controller/command',           [0.4,-0.4,0.0]],
                ['legRF2', '/aibo_tc/legRF2_position_controller/command',           [0.4,0.0]],
                ['legRF3', '/aibo_tc/legRF3_position_controller/command',           [0.4,0.0]],
                ['mouth_joint', '/aibo_tc/mouth_joint_position_controller/command', [0.4,0.0]],
                ['neck_joint', '/aibo_tc/neck_joint_position_controller/command',   [0.4,-0.4,0.0]],
                #['tailPan', '/aibo_tc/tailPan_position_controller/command',         [0.4,-0.4,0.0]],
                #['tailTilt', '/aibo_tc/tailTilt_position_controller/command',       [0.4,-0.4,0.0]],
            ]

for i in range(len(joint_list)):
    tm.print_header("\nTest motion - {}".format(joint_list[i][0]))
    topic_command = joint_list[i][1]
    topic_state = '/aibo_tc/joint_states'
    joint_name = joint_list[i][0]
    point_list = joint_list[i][2]
    results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.1, wait_motion_time=1.5))


tm.print_header("\nTest Motion - Movement. ")

command_list = [    ["Forward",     Twist(linear=Vector3( 0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Reverse",     Twist(linear=Vector3(-0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Turn left", Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.2))], 
                    ["Turn right",Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.2))],
                    ["Stop",          Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/aiboERS7/cmd_vel', 'aiboERS7'))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)