#!/usr/bin/env bash
#-----------------------------------------------------------------
# Running this script will:
# 1. Launch the Gazebo simulation, at line 12
# 2. Run the testing script, at line 18
# 3. Once the test is done, it will kill everything. 
#    Extra step is made to ensure gzserver is really stopped. 
# 4. Exit with code 0 if test is PASS and 1 if FAIL. 
#-----------------------------------------------------------------

# Launch the Gazebo simulation and wait for it to load
roslaunch summit_xl_gazebo main.launch &
gazebo_pid=$!
echo "\nLoading Gazebo simulation (PID:$gazebo_pid)"
sleep 30

# Run the simulation tester
rosrun robot_simulations_testing test_summit_xl.py &
test_pid=$!
echo "\nTest script started (PID:$test_pid)"

if wait $test_pid; then
	test_res=0
    #echo "Test $test_pid success"
else
	test_res=1
    #echo "Test $test_pid fail"
fi
sleep 5
# Simulation done, kill Gazebo
echo "\nTerminating Gazebo."
kill $gazebo_pid > /dev/null 2>&1
wait $gazebo_pid
gzserver_pids=$(pgrep gzserver)
#echo $gzserver_pids
for i in $gzserver_pids; do
	#echo "Killing gzserver $i"
	kill -9 $i > /dev/null 2>&1
	wait $i
done
echo "\nTest done."

# Exit with the appropriate code
#echo $test_res
exit $test_res
