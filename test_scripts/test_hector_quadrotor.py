#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''
tm.print_header("\nTest sensors. ")
sensor_list = [ ["Front RGB camera", '/front_cam/camera/image', Image],
                ["Laser sensor", '/scan', LaserScan],
                ["IMU",'/raw_imu', Imu],
                ["Sonar", '/sonar_height', Range],
                ["NavSat Fix",'/fix', NavSatFix],
                ["NavSat Vel",'/fix_velocity', Vector3Stamped],
                ["Compass", '/magnetic', Vector3Stamped],
              ]
results.append(tm.test_sensor(sensor_list))


'''---------- Test Motion ---------'''
tm.print_header("\nTake off. ")
data = Empty()
tm.publish('/takeoff', data)
rospy.sleep(3)

tm.print_header("\nTest Motion. ")

command_list = [    ["Reverse",   Twist(linear=Vector3(-0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Forward", 	Twist(linear=Vector3( 0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Pan right", Twist(linear=Vector3( 0.0,-0.5, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Pan left",  Twist(linear=Vector3( 0.0, 0.5, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Turn left", Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.3))], 
                    ["Turn right",Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.3))],
                    ["Ascend",    Twist(linear=Vector3( 0.0, 0.0, 0.5),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Descend",   Twist(linear=Vector3( 0.0, 0.0,-0.5),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Stop", 		  Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/cmd_vel', 'quadrotor'))

tm.print_header("\nLanding. ")
data = Empty()
tm.publish('/land', data)

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)