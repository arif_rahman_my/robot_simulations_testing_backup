#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Sensors ---------'''

tm.print_header("\nTest sensors. ")
sensor_list = [ ["Laser sensor", '/front_laser/scan', LaserScan],
                ["Front RGB camera", '/front_camera/rgb/image_raw', Image],
                ["Front Depth camera", '/front_camera/depth/image_raw', Image],
                ["Head RGB camera", '/head_camera/rgb/image_raw', Image],
                ["Head Depth camera", '/head_camera/depth/image_raw', Image],
                ["IMU", '/imu/data', Imu],
              ]
results.append(tm.test_sensor(sensor_list))

'''---------- Test Drive Motion ---------'''

tm.print_header("\nTest drive motion. ")

command_list = [    ["Forward",     Twist(linear=Vector3( 0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Reverse",     Twist(linear=Vector3(-0.5, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
                    ["Turn left",   Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.5))], 
                    ["Turn right",  Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0,-0.5))],
                    ["Stop",        Twist(linear=Vector3( 0.0, 0.0, 0.0),angular=Vector3( 0.0, 0.0, 0.0))], 
               ]

results.append(tm.test_motion_Twist_Position(command_list, '/rb1/mobile_base_controller/cmd_vel','rb1'))

'''---------- Test Arm Motion ---------'''

tm.print_header("\nTest arm motion. ")
topic = "/rb1/mico_arm_controller/follow_joint_trajectory"
joint_names = ["mico_joint_1", "mico_joint_2", "mico_joint_3", "mico_joint_4", "mico_joint_5", "mico_joint_6"]

joint_positions = [ [ 0.00,-0.78,-0.44,-1.63, 0.00, 0.00],
                    [ 1.57, 0.00,-1.57,-1.57,-1.57,-1.57],
                    [ 1.57,-0.55,-2.14,-1.13,-3.13,-1.94],
                    [ 0.00, 0.51,-1.38, 0.56, 2.19, 2.50],
                    [-1.57,-1.00, 0.60, 1.57, 1.57, 1.57],
                    [ 0.00, 0.51,-0.11, 0.03,-0.01,-0.03],
                  ]
results.append(tm.test_motion_FollowJointTrajectoryAction(topic, joint_names, joint_positions))


tm.print_header("\nTest gripper motion. ")
topic = "/rb1/gripper_controller/follow_joint_trajectory"
joint_names = ["mico_joint_finger_1", "mico_joint_finger_2", "mico_joint_finger_3"]
joint_positions = [ [0.3,0.3,0.3],
                    [0.7,0.7,0.7],
                    [0.4,0.4,0.4],
                  ]
results.append(tm.test_motion_FollowJointTrajectoryAction(topic, joint_names, joint_positions))

tm.print_header("\nTest head motion - tilt. ")
topic_command = '/rb1/head_tilt_controller/command'
topic_state = '/rb1/joint_states'
joint_name = 'head_tilt_joint'
point_list = [ 0.5,
              -0.5,
               0.0,
             ]
results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list))

tm.print_header("\nTest head motion - pan. ")
topic_command = '/rb1/head_pan_controller/command'
topic_state = '/rb1/joint_states'
joint_name = 'head_pan_joint'
point_list = [ 0.5,
              -0.5,
               0.0,
             ]
results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list))

tm.print_header("\nTest torso motion. ")
topic_command = '/rb1/torso_controller/command'
topic_state = '/rb1/joint_states'
joint_name = 'torso_slider_joint'
point_list = [ 0.2,
               0.36,
               0.15,
               0.0,
             ]
results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.1))

'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)