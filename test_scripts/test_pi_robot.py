#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Motion ---------'''
joint_list = [  ['head_pan_joint', '/pi_robot/head_pan_joint_position_controller/command',                              [0.4,-0.4,0.0]],
                ['head_tilt_joint', '/pi_robot/head_tilt_joint_position_controller/command',                            [0.4,-0.4,0.0]],
                ['left_elbow_joint', '/pi_robot/left_elbow_joint_position_controller/command',                          [0.4,-0.4,0.0]],
                ['left_shoulder_forward_joint', '/pi_robot/left_shoulder_forward_joint_position_controller/command',    [0.4,-0.4,0.0]],
                ['left_shoulder_up_joint', '/pi_robot/left_shoulder_up_joint_position_controller/command',              [0.4,0.0,-0.4,0.0]],
                ['left_wrist_joint', '/pi_robot/left_wrist_joint_position_controller/command',                          [0.4,-0.4,0.0]],
                ['right_elbow_joint', '/pi_robot/right_elbow_joint_position_controller/command',                        [0.4,-0.4,0.0]],
                ['right_shoulder_forward_joint', '/pi_robot/right_shoulder_forward_joint_position_controller/command',  [0.4,-0.4,0.0]],
                ['right_shoulder_up_joint', '/pi_robot/right_shoulder_up_joint_position_controller/command',            [0.4,0.0,-0.4,0.0]],
                ['right_wrist_joint', '/pi_robot/right_wrist_joint_position_controller/command',                        [0.4,-0.4,0.0]],
                ['torso_joint', '/pi_robot/torso_joint_position_controller/command',                                    [0.4,-0.4,0.0]],
             ]

for i in range(len(joint_list)):
    tm.print_header("\nTest Motion - {}".format(joint_list[i][0]))
    topic_command = joint_list[i][1]
    topic_state = '/pi_robot/joint_states'
    joint_name = joint_list[i][0]
    point_list = joint_list[i][2]
    results.append(tm.test_motion_Float(topic_command, topic_state, joint_name, point_list, thres_joint_position_val=0.2, wait_motion_time=3.0))


'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)