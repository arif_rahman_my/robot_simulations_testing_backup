#! /usr/bin/env python

import rospy
import math
import sys
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import GetWorldProperties
from sensor_msgs.msg import * 
from geometry_msgs.msg import *
from nav_msgs.msg import * 
from trajectory_msgs.msg import *
from control_msgs.msg import * 
from std_msgs.msg import *
from colors import colors
from Test_Model import Test_Model 

tm = Test_Model(debug=True)
results = []

'''---------- Wait for Gazebo ---------'''
tm.print_header("\nWait for gazebo to load")
tm.wait_for_gazebo_load()


'''---------- Test Arm Motion ---------'''
tm.print_header("\nTest arm motion. ")
action_topic = "/arm_controller/follow_joint_trajectory"

joint_names = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]

joint_positions = [ [ 1.57,-1.00, 1.57, 1.57, 1.57, 1.57],
                    [-1.57,-1.57,-1.57,-1.57,-1.57,-1.57],
                    [-1.57, 0.25,-1.00,-1.00, 0.87,-1.00],
                    [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],

                  ]
results.append(tm.test_motion_FollowJointTrajectoryAction(action_topic, joint_names, joint_positions))


'''---------- Final -------------'''
if(sum(results)/len(results) == 1.0):
  tm.print_header("\n\nTESTS PASSED.")
  sys.exit(0)
else:
  tm.print_header("\n\nTESTS FAILED.")
  sys.exit(1)